<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name'=>'alvaro',
            'email'=>'loro102@gmail.com',
            'password'=>bcrypt('procesator')
        ]);

        User::factory(99)->create();
    }
}
